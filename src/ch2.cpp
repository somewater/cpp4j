#include <iostream>
#include <climits>
#include "ch2.h"
#include <vector>
#include <string>
#include <cstring>
using namespace std;

int max2(int x, int y = 0);
int max2(double, double = 0);
string makeLongString( int n );
void swap(int & x, int & y);
int binarySearch( const vector<int> & arr, int x );
vector<string> selectLongest(const vector<string> & words);
void printWords(string prefix, const vector<string> & words);

int simplemax(int x, int y) {
	return x > y ? x : y;
}
inline int supermax(int x, int y) {
	return x > y ? x : y;
}

int ch2() {
	int x = 1;
	int y = 2;
	cout << "max(" << x << "," << y << ") = " << max2(x, y) << endl;
	cout << endl;
	cout << "max(" << x << "," << "<default>" << ") = " << max2(double(x)) << endl;
	cout << endl;

    std::vector<int> ints;
    cout << "ints.size = " << ints.size() << endl;
    ints.push_back(10);
    ints.push_back(20);
    ints.push_back(30);
    ints.push_back(40);
    cout << "ints[3] = " << ints[3] << endl;
    cout << endl;

    vector<int> squares;
    squares.resize(4);
    for( int i = 0; i < squares.size(); i++ )
    	squares[i] = ( i * i );
    for( int j = 0; j < squares.size( ); j++ )
    	cout << j << " squared is " << squares[ j ] << endl;

    string s = makeLongString(300);
    string s_copy = s;
    s[3] = 'D';
    char s2[] = "helloФ";
    cout << "s.size = " << s.size() << ", s.length  = " << s.length()
    		<< ", content like '" << makeLongString(10) << "'"
			<< ", substr(2,5) = " << s.substr(2,5)
    		<< ", substr(2,4) = " << s.substr(2,4)
			<< ", s[0] = " << s[0] << ", s[5] = " << s[5]
		    << ", s_copy = " << s_copy << endl;
    cout << "len(s2) = " << strlen(s2) << ", content = '" << s2 << "'" << endl;
    cout << endl;

    int md[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    cout << "sizeof(md) = " << sizeof(md) << ", sizeof(md[0]) = " << sizeof(*md) << ", md = ";
    float md1 = 100500;
    int md1_i = (int) md1;
    swap(md[0], md1_i);
    md1 = md1_i;
    for (int i = 0; i < sizeof(md) / sizeof(*md); i++)
    	cout << "(" << i << "=" << *(md + i) << ") ";
	cout << "md1 = " << md1 << ", md1_i = " << md1_i << endl;
    cout << endl;

    vector<int> v;
    cout << "START,,," << endl;
    for( int i = 0; i < 300000; i++ )
        v.push_back( i * i );
    for( int j = 1000000; j < 1050000; j++ )
        if( binarySearch( v, j ) >= 0 )
            {}

    vector<string> words;
    cout << "Pleas type some words (type \"0\" for stop):" << endl;
    while (true) {
    	string in;
    	cin >> in;
    	if (in.empty() || in == "0") {
    		break;
    	} else {
    		cout << "new word \"" << in << "\"" << endl;
    		words.push_back(in);
    	}
    }
    vector<string> longestWords = selectLongest(words);
    printWords("words", words);
    printWords("longestWords", longestWords);

    cout << "__END__" << endl;

    return 0;
}

int max2(int x, int y) {
	cout << "using (int, int) version of max" << endl;
	return x > y ? x : y;
}

int max2(double x, double y) {
	cout << "using (double, double) version of max" << endl;
	return x > y ? x : y;
}

void swap(int & x, int & y) {
	int tmp = x;
	x = y;
	y = tmp;
}

string makeLongString( int n )
{
	string result = "";
	for( int i = 0; i < n; i++ )
		result += "Ф";
	return result;
}

int binarySearch( const vector<int> & arr, int x )
{
    int low = 0, high = arr.size( ) - 1;

    while( low <= high )
    {
        int mid = ( low + high ) / 2;
        if( arr[ mid ] == x )
            return mid;
        else if( x < arr[ mid ] )
            high = mid - 1;
        else
            low = mid + 1;
    }

    return -1; // not found
}

void printWords(string prefix, const vector<string> & words) {
	cout << prefix << ": ";
	for (int i = 0; i < words.size(); i++)
		cout << "'" << words[i] << "' ";
	cout << endl;
}

vector<string> selectLongest(const vector<string> & words) {
	vector<string> result;
	int longest = 0;
	for (int i = 0; i < words.size(); i++)
		if (words[i].size() > longest)
			longest = words[i].size();
	for (int i = 0; i < words.size(); i++)
		if (words[i].size() == longest)
			result.push_back(words[i]);
	return result;
}
