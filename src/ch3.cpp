#include <iostream>
#include <string>
using namespace std;

int ch3();

void generateString() {
	string *s = new string("Hello world");
	string *s2 = s;
	int len = s->length();
	len++;
	delete s;
	*s2 = "hi all";

	cout << "s=" << *s2 << endl;
	cout << "s2=" << *s2 << endl;
}

string * dup(const string & str) {
	static string ret = str + str;
	string * s = & ret;
	return & ret;
}

int ch3() {
	int a, b;
	a = 78;
	int *ptr;
	int **ptrptr;

	ptr = &a;
	ptrptr = &ptr;

    cout << "** ptrptr = " << ** ptrptr << endl;
    cout << " &* ptr = " << &*ptr << endl;
    cout << " *& a = " << *&a << endl;

    cout << "ptr -> " << ptr << endl;
    cout << "*ptr -> " << *ptr << endl;
    cout << "*ptr == a -> " << (*ptr == a) << endl;
    cout << "ptr == &a -> " << (ptr == &a) << endl;
    cout << "&ptr -> " << &ptr << endl;
    cout << "*&a -> " << *&a << endl;
    cout << "**&ptr -> " << **&ptr << endl;

	for (int i = 0; i < 1; i++) {
		string * s = dup("Hello");
		if (i < 5)
			cout << "*s = " << *s << endl;
	}

	cout << "End" << endl;
	string s;
	cin >> s;
	return 0;
}
