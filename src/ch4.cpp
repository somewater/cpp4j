#include <iostream>
#include <climits>
#include "ch4.h"
#include <vector>
#include <string>
#include <cstring>
using namespace std;

struct Foo {
	char type;
	int value;
};
void testWIthStruct();
void testWithIntCell();
class IntCell
{
	public:
	IntCell( int initialValue = 0 ) {
		storedValue = initialValue;
	}
	int getValue( ) const {
		return storedValue;
	}
	void setValue( int val ) {
		storedValue = val;
	}
	private:
	int storedValue;
};

int ch4() {
	testWIthStruct();

	testWithIntCell();

	return 0;
}

void testWIthStruct() {
	struct Bar {
		int min;
		int sec;
	} bar;
	bar.min = 4;

	Foo foo;
	Foo * f = &foo;
	Foo * f2 = new Foo;
	f2->type = 'T';
	foo.value = 3434;
	//cout << "foo.type = " << f2->type << ", foo.value = " << foo.value << ". bar.min = " << bar.min <<  endl;
	delete f2;
}

bool containsZero( const vector<IntCell> & arr )
{
	for( int i = 0; i < arr.size( ); i++ )
	if( arr[ i ].getValue( ) == 0 )
		return true;
	return false;
}

void testWithIntCell() {
	IntCell m1;
	IntCell m2 = 37;
	IntCell m3( 55 );
	cout << "m1=" << m1.getValue( ) << ", m2=" << m2.getValue( ) << ", m3=" << m3.getValue( ) << endl;

	vector<IntCell> cells;
	cells.push_back(m1);
	cells.push_back(m2);
	cells.push_back(m3);
	cout << "Has value=0 cell ? " <<  containsZero(cells) << endl;

	m1 = m2;
	m2.setValue( 40 );
	cout << "m1=" << m1.getValue( ) << ", m2=" << m2.getValue( ) << endl;
}
